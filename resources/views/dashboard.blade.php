<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                @if ($user->authorized)
                    <div class="flex justify-end items-center h-10">
                        <a href="{{ route('create') }}" class="inline-flex items-center px-2 py-1 bg-indigo-500 focus:outline-none text-white hover:bg-indigo-600 font-medium rounded-lg text-sm px-6 py-3 mb-3">
                            {{ __('New task') }}
                        </a>
                    </div>
                    <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-4">
                        @livewire('tasks-table')
                    </div>
                @else
                    <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-4">
                        @livewire('tasks-table-view')
                    </div>
                @endif
        </div>
    </div>
</x-app-layout>
