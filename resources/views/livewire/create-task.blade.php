<x-jet-form-section submit="submit">
    <x-slot name="title">
        <div class="col">
            {{ __('Task Information') }}
        </div>
    </x-slot>

    <x-slot name="description">
        {{ __('Complete the followings fields to create a new task') }}
    </x-slot>

    <x-slot name="form">
        <!-- description -->
        <div class="col-span-6 sm:col-span-6">
            <x-jet-label for="description" value="{{ __('Description') }}" />
            <x-jet-input id="description" type="text" class="mt-1 block w-full" wire:model.defer="description" autocomplete="description" />
            <x-jet-input-error for="description" class="mt-2" />
        </div>
        <!-- group -->
        <div class="col-span-6 sm:col-span-6">
            <x-jet-label for="group" value="{{ __('Group') }}" />
            <select id="group" class="mt-1 block w-full" wire:model.defer="group" name="group">
                <option value="" hidden>{{ __('Choose an option') }}</option>
                @foreach($tasksGroups as $tasksGroup)
                    <option value="{{ $tasksGroup->id }}">{{ $tasksGroup->name }}</option>
                @endforeach
            </select>
            <x-jet-input-error for="group" class="mt-2" />
        </div>
        <!-- deadline -->
        <div class="col-span-6 sm:col-span-6">
            <x-jet-label for="deadline" value="{{ __('Deadline') }}" />
            <x-jet-input id="deadline" type="date" class="mt-1 block w-full" wire:model="deadline" autocomplete="deadline" />
            <x-jet-input-error for="deadline" class="mt-2" />
        </div>
        <!-- frequency -->
        <div class="col-span-6 sm:col-span-6">
            <x-jet-label for="frequency" value="{{ __('Frequency') }}" />
            <div class="flex mt-2">
                <div class="flex items-center">
                    <input class="w-4 h-4 bg-gray-100 border-gray-300 rounded" name="frequency" autocomplete="off" id="1" value="1" type="radio" wire:model="frequency">
                    <label class="ml-1 mr-3 text-sm font-medium text-gray-900 dark:text-gray-300" for="1">Once
                    </label>

                    <input class="w-4 h-4 bg-gray-100 border-gray-300 rounded" name="frequency" autocomplete="off" id="2" value="2" type="radio" wire:model="frequency">
                    <label class="ml-1 mr-3 text-sm font-medium text-gray-900 dark:text-gray-300" for="2">Daily
                    </label>

                    <input class="w-4 h-4 bg-gray-100 border-gray-300 rounded" name="frequency" autocomplete="off" id="3" value="3" type="radio" wire:model="frequency">
                    <label class="ml-1 mr-3 text-sm font-medium text-gray-900 dark:text-gray-300" for="3">Weekly
                    </label>                                        

                    <input class="w-4 h-4 bg-gray-100 border-gray-300 rounded" name="frequency" autocomplete="off" id="4" value="4" type="radio" wire:model="frequency">
                    <label class="ml-1 mr-3 text-sm font-medium text-gray-900 dark:text-gray-300" for="4">Monthly
                    </label>

                    <input class="w-4 h-4 bg-gray-100 border-gray-300 rounded" name="frequency" autocomplete="off" id="5" value="5" type="radio" wire:model="frequency">
                    <label class="ml-1 mr-3 text-sm font-medium text-gray-900 dark:text-gray-300" for="5">Annually
                    </label>                    
                </div>
            </div>
            <x-jet-input-error for="frequency" class="mt-2" />
        </div> 

        @if($frequency != '1')
        <!-- schedule -->
        <div class="col-span-6 sm:col-span-3">
            <x-jet-label for="from" value="{{ __('From') }}" />
            <x-jet-input id="from" type="date" class="mt-1 block w-full" wire:model="from" autocomplete="from" />
            <x-jet-input-error for="from" class="mt-2" />
        </div>  
        <div class="col-span-6 sm:col-span-3">
            <x-jet-label for="to" value="{{ __('To') }}" />
            <x-jet-input id="to" type="date" class="mt-1 block w-full" wire:model="to" autocomplete="to" />
            <x-jet-input-error for="to" class="mt-2" />
        </div>  
        @endif
    </x-slot>

    <x-slot name="actions">
        <x-jet-action-message class="mr-3" on="saved">
            {{ __('Saved.') }}
        </x-jet-action-message>

        <x-jet-button wire:loading.attr="disabled">
            {{ __('Save') }}
        </x-jet-button>
    </x-slot>
</x-jet-form-section>
