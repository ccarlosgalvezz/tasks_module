<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Add task') }}
        </h2>
        <div class="flex justify-end items-center">
            <a href="{{ route('dashboard') }}" class="inline-flex items-center px-2 py-1 bg-indigo-500 focus:outline-none text-white hover:bg-indigo-600 font-medium rounded-lg text-sm px-5 py-2.5">
                {{ __('Back') }}
            </a>
        </div>    
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @livewire('create-task')
        </div>
    </div>
</x-app-layout>