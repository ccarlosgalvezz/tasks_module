<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', [Controller::class, 'index'])->name('dashboard');
    Route::get('/create', [Controller::class, 'create'])->name('create');
    Route::get('tasks/update/{id}', [Controller::class, 'update'])->name('update');
    Route::get('tasks/delete/{id}', [Controller::class, 'delete'])->name('delete');
    Route::get('tasks/cron', [Controller::class, 'cron'])->name('cron');
});
