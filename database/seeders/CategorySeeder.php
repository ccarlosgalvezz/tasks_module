<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(['name' => 'Tasks Today']);
        Category::create(['name' => 'Tasks Tomorrow']);
        Category::create(['name' => 'Tasks Next Week']);
        Category::create(['name' => 'Tasks in the Near Future']);
        Category::create(['name' => 'Tasks in the Future']);
    }
}
