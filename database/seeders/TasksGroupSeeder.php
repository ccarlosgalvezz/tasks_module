<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\TasksGroup;


class TasksGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'House',
                'description' => 'Tasks inside home',
            ],
            [
                'name' => 'Outside',
                'description' => 'Tasks outside our property fence',
            ],
            [
                'name' => 'Garden',
                'description' => 'Tasks in the garden',
            ]
        ];

        $data = json_decode(json_encode($data));
        foreach ($data as $item) {

            TasksGroup::create([
                'name' => $item->name,
                'description' => $item->description,
            ]);
        }
    }
}
