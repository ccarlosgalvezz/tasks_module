<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Task;
use Carbon\Carbon;


class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'description' => 'Doing the laundry',
                'tasks_group_id' => '1',
                'frequency_id' => '1',
                'deadline_at' => Carbon::now()->format('Y-m-d'),
                'category_id' => '1',
                'status_id' => '2'
            ],
            [
                'description' => 'Mopping floors',
                'tasks_group_id' => '1',
                'frequency_id' => '2',
                'deadline_at' => Carbon::now()->addDay()->format('Y-m-d'),
                'user_id' => '2',
                'category_id' => '2',
                'status_id' => '1'
            ],
            [
                'description' => 'Washing the family car',
                'tasks_group_id' => '2',
                'frequency_id' => '3',
                'deadline_at' => Carbon::now()->addWeek()->format('Y-m-d'),
                'category_id' => '3',
                'status_id' => '1'
            ],
            [
                'description' => 'Walking family pets',
                'tasks_group_id' => '2',
                'frequency_id' => '4',
                'deadline_at' => Carbon::now()->addWeeks(2)->format('Y-m-d'),
                'category_id' => '4',
                'status_id' => '1'
            ],
            [
                'description' => 'Watering plants',
                'tasks_group_id' => '3',
                'frequency_id' => '5',
                'deadline_at' => Carbon::now()->addMonth()->format('Y-m-d'),
                'category_id' => '5',
                'status_id' => '1'
            ],
            [
                'description' => 'Sweeping leafs',
                'tasks_group_id' => '3',
                'frequency_id' => '1',
                'deadline_at' => Carbon::now()->addMonth(3)->format('Y-m-d'),
                'category_id' => '5',
                'status_id' => '1'
            ]            
        ];

        $data = json_decode(json_encode($data));
        foreach ($data as $item) {

            Task::create([
                'description' => $item->description,
                'tasks_group_id' => $item->tasks_group_id,
                'frequency_id' => $item->frequency_id,
                'deadline_at' => $item->deadline_at,
                'category_id' => $item->category_id,
                'status_id' => $item->status_id
            ]);
        }

    }
}
