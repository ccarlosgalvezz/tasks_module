<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Frequency;

class FrequencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Once',
                'days' => 0,
            ],
            [
                'name' => 'Daily',
                'days' => 1,
            ],
            [
                'name' => 'Weekly',
                'days' => 7,
            ],
            [
                'name' => 'Monthly',
                'days' => 30,
            ],
            [
                'name' => 'Annually',
                'days' => 365,
            ],
        ];

        $data = json_decode(json_encode($data));
        foreach ($data as $item) {

            Frequency::create([
                'name' => $item->name,
                'days' => $item->days,
            ]);
        }
    }
}
