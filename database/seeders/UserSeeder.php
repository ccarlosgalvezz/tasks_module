<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'user1',
            'email' => 'user1@test.com',
            'password' => Hash::make('test1'),
            'authorized' => true
        ]);
        User::create([
            'name' => 'user2',
            'email' => 'user2@test.com',
            'password' => Hash::make('test2'),
            'authorized' => false
        ]);
    }
}
