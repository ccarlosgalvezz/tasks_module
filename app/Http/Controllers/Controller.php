<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use App\Models\Task;
use DB;
use Auth;
use Carbon\Carbon;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        $user = Auth::user();

        $todayTasks = Task::where('deadline_at', Carbon::now()->format('Y-m-d'))->select('id')->get();
        $tomorrowTasks = Task::whereDate('deadline_at', Carbon::now()->addDay()->format('Y-m-d'))->select('id')->get();
        $nextWeekTasks = Task::whereDate('deadline_at', Carbon::now()->addDays(7)->format('Y-m-d'))->select('id')->get();
        $nearTasks = Task::whereDate('deadline_at', '>', Carbon::now()->addDays(7)->format('Y-m-d'))->select('id')->get();
        $futureTasks = Task::whereDate('deadline_at', '>', Carbon::now()->addDays(30)->format('Y-m-d'))->select('id')->get();
        foreach ($todayTasks as $todayTask) {
            DB::table('tasks')->where('id',$todayTask->id)->update([
                'category_id' => '1',
            ]);
        }
        foreach ($tomorrowTasks as $tomorrowTask) {
            DB::table('tasks')->where('id',$tomorrowTask->id)->update([
                'category_id' => '2',
            ]);
        }
        foreach ($nextWeekTasks as $nextWeekTask) {
            DB::table('tasks')->where('id',$nextWeekTask->id)->update([
                'category_id' => '3',
            ]);
        }
        foreach ($nearTasks as $nearTask) {
            DB::table('tasks')->where('id',$nearTask->id)->update([
                'category_id' => '4',
            ]);
        }
        foreach ($futureTasks as $futureTask) {
            DB::table('tasks')->where('id',$futureTask->id)->update([
                'category_id' => '5',
            ]);
        }

        return view('dashboard', compact('user'));
    }

    public function create()
    {
        return view('create');
    }

    public function update($id)
    {
        $task = Task::find($id);
        if ($task->status_id == 1) {
            DB::table('tasks')
            ->where('id', $id)
            ->update(['status_id' => 2]);
            session()->flash('flash.banner', 'Task marked as completed!');
            session()->flash('flash.bannerStyle', 'success');
        }
        else {
            session()->flash('flash.banner', 'This task was already done!');
            session()->flash('flash.bannerStyle', 'danger');
        }
        return redirect()->route('dashboard');
    }

    public function delete($id)
    {
        $task = Task::find($id);
        $task->delete();

        session()->flash('flash.banner', 'Task deleted');
        session()->flash('flash.bannerStyle', 'success');
        return redirect()->route('dashboard');
    }

}
