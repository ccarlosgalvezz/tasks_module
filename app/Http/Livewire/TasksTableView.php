<?php

namespace App\Http\Livewire;

use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

use Rappasoft\LaravelLivewireTables\Views\Filters\DateFilter;

use App\Models\Task;
use App\Models\Category;


class TasksTableView extends DataTableComponent
{
    public function configure(): void
    {
        $this->setPrimaryKey('id')
            ->setColumnSelectDisabled()
            ->setPaginationStatus(false)
            ->setDefaultSort('deadline_at', 'asc');
    }

    public function columns(): array
    {
        return [
            Column::make(__('Id'), "id")
                ->sortable(),
            Column::make(__('Description'), "description")
                ->format(
                    fn($value, $row, Column $column) => '<strong>'.$row->description.'</strong>'
                )
                ->html()
                ->sortable()
                ->searchable(),
            Column::make(__('Group'), "tasksGroup.name")
                ->sortable(),
            Column::make(__('Deadline'), "deadline_at")
                ->sortable(),
            Column::make(__('Category'), "category.name")
                ->sortable(),
            Column::make(__('Frequency'), "frequency.name")
                ->sortable(),
            Column::make(__('Status'), "status_id")
                ->sortable()
                ->format(function ($value) {
                    if ($value == 1) {
                        return $value = __('Pending');
                    } else {
                        return $value = __('Completed');
                    }
            }),
        ];
    }


    public function builder(): Builder
    {
        return Task::query()
        ->join('frequencies', 'frequencies.id', '=', 'tasks.frequency_id');
    }

    public function filters(): array
    {
        return [
            MultiSelectFilter::make(__('Category'))
                ->options(
                    Category::query()
                        ->orderBy('id')
                        ->get()
                        ->keyBy('id')
                        ->map(fn ($category) => $category->name)
                        ->toArray()
                )
                ->filter(function(Builder $builder, array $values) {
                    $builder->whereHas('category', fn ($query) => $query->whereIn('categories.id', $values));
                }),
            DateFilter::make(__('From'))
                ->filter(function (Builder $builder, string $value) {
                    $builder->where('tasks.deadline_at', '>=', $value);
                }),
        ];
    }
}
