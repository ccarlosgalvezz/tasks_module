<?php

namespace App\Http\Livewire;

use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Columns\ButtonGroupColumn;
use Rappasoft\LaravelLivewireTables\Views\Columns\LinkColumn;

use Rappasoft\LaravelLivewireTables\Views\Filters\DateFilter;
use Rappasoft\LaravelLivewireTables\Views\Filters\MultiSelectFilter;

use App\Models\Task;
use App\Models\Category;


class TasksTable extends DataTableComponent
{
    public function configure(): void
    {
        $this->setPrimaryKey('id')
            ->setColumnSelectDisabled()
            ->setPaginationStatus(false)
            ->setDefaultSort('deadline_at', 'asc');
    }

    public function columns(): array
    {
        return [
            Column::make(__('Id'), "id")
                ->sortable(),
            Column::make(__('Description'), "description")
                ->format(
                    fn($value, $row, Column $column) => '<strong>'.$row->description.'</strong>'
                )
                ->html()
                ->sortable()
                ->searchable(),
            Column::make(__('Group'), "tasksGroup.name")
                ->sortable(),
            Column::make(__('Deadline'), "deadline_at")
                ->sortable(),
            Column::make(__('Category'), "category.name")
                ->sortable(),
            Column::make(__('Frequency'), "frequency.name")
                ->sortable(),
            Column::make(__('Status'), "status_id")
                ->sortable()
                ->format(function ($value) {
                    if ($value == 1) {
                        return $value = __('Pending');
                    } else {
                        return $value = __('Completed');
                    }
            }),
            ButtonGroupColumn::make('Actions')
                ->attributes(function ($row) {
                    return [
                        'class' => 'd-inline-flex'
                    ];
                })
                ->buttons([
                    LinkColumn::make('Action')
                        ->title(fn ($row) => __('Done'))
                        ->location(fn ($row) => route('update', $row))
                        ->attributes(fn ($row) => [
                            'class' => 'inline-flex items-center px-2 py-1 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25 transition'
                        ]),
                    LinkColumn::make('Action')
                        ->title(fn ($row) => __('Delete'))
                        ->location(fn ($row) => route('delete', $row))
                        ->attributes(fn ($row) => [
                            'class' => 'inline-flex items-center px-2 py-1 bg-gray-500 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25 transition'
                        ]),
                ]),
        ];
    }


    public function builder(): Builder
    {
        return Task::query()
        ->join('frequencies', 'frequencies.id', '=', 'tasks.frequency_id');
    }

    public function filters(): array
    {
        return [
            MultiSelectFilter::make(__('Category'))
                ->options(
                    Category::query()
                        ->orderBy('id')
                        ->get()
                        ->keyBy('id')
                        ->map(fn ($category) => $category->name)
                        ->toArray()
                )
                ->filter(function(Builder $builder, array $values) {
                    $builder->whereHas('category', fn ($query) => $query->whereIn('categories.id', $values));
                }),
            DateFilter::make(__('From'))
                ->filter(function (Builder $builder, string $value) {
                    $builder->where('tasks.deadline_at', '>=', $value);
                }),
        ];
    }
}
