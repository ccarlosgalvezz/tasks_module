<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Task;
use App\Models\TasksGroup;
use App\Models\Frequency;
use App\Models\Status;
use Carbon\Carbon;
use DB;

class CreateTask extends Component
{

    public $description, $group, $deadline, $frequency, $status;
    public $yesterday;
    public $from, $to;

    public function render()
    {
        $this->yesterday = Carbon::yesterday();
        
        $tasksGroups = TasksGroup::get();
        $frequencies = Frequency::get();
        $stat = Status::get();

        return view('livewire.create-task', compact('tasksGroups','frequencies','stat'));
    }

    public function rules()
    {
        return [
            'description' => ['required', 'string', 'max:70'],
            'group' => ['required'],
            'deadline' => ['required', 'date', 'after:yesterday'],
            'frequency' => ['required'],
        ];
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function submit()
    {
        $this->validate();

        try {

            DB::beginTransaction();

            $task = new Task();
            $task->description = $this->description;
            $task->tasks_group_id = $this->group;
            $task->deadline_at = $this->deadline;
            $task->frequency_id = $this->frequency;
            $task->status_id = 1;
            $task->save();

            DB::commit();

            $this->task = $task;
          
            if ($this->frequency == 2) {
                $diff = Carbon::parse($this->to)->diffInDays(Carbon::parse($this->from));
                for ($i=1; $i<$diff; $i++) {
                    DB::table('tasks')->insert([
                        ['description' => $this->description,
                        'tasks_group_id' => $this->group,
                        'deadline_at' => Carbon::parse($this->deadline)->addDays($i),
                        'frequency_id' => $this->frequency,
                        'status_id' => '1'],
                    ]);
                }
                session()->flash('flash.banner', 'Daily task created successfully!');
            } else {
                session()->flash('flash.banner', 'Task created successfully!');
            }
            session()->flash('flash.bannerStyle', 'success');

            return redirect('dashboard');

        
            } catch (\Exception $e) {
                DB::rollBack();
                dd($e);
            }
  

    }

}
