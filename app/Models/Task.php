<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    public function tasksGroup()
    {
        return $this->belongsTo(TasksGroup::class);
    }
    public function frequency()
    {
        return $this->hasOne(Frequency::class);
    }
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

}
