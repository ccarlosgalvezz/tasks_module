<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

# Tasks Manager

## About this module

Made with Laravel v.8 web application framework.
[Doc](https://laravel.com/docs/8.x)

### Stack:
Defined as mandatory on the test document instructions:
- [Laravel Jetstream with Livewire](https://jetstream.laravel.com/3.x/introduction.html#livewire-blade)
- [Tailwind](https://tailwindcss.com/docs/installation)

Other components:
- [Laravel Livewire Tables](https://rappasoft.com/packages/laravel-livewire-tables). To show and manage models data in table format, with sorting and grouping funcionalities.
- [Carbon](https://carbon.nesbot.com/). To manipulate dates.

For local development I have chosen the following stack because I am used to it:
- PHP 8.1.3
- MySQL
- Laragon
- DBeaver
- Visual Studio Code (with Tailwind CSS IntelliSense extension)

The database diagram is in the root path of the project (tasks_module_diagram.png).

This repository will be in Gitlab.

## How to run the module
1. Check environment
2. Download project from repository
3. Run migrations and seeders
4. Open it in the browser
5. To try the authorization system:
    - Log in as user2@test.com with password test2 just to see tasks as non-authorized user
    - Log in as user1@test.com with password test1 to manage tasks as authorized user
6. On dashboard page:
    - use search input to find tasks by description
    - try filters to check grouping funcionalities
    - click on headers to sorting
    - click on buttons to check other funcionalities